<?php
class Curl
{
	public function call($url, $post = false, $post_param = false, $header = false, $cookie='', $proxy = '', $get_header = false)
	{
			
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL,$url);
		
		if($proxy){
			curl_setopt($ch, CURLOPT_PROXY, $proxy); 
		}
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Linux; Android 8.0; Pixel 2 Build/OPD3.170816.012) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Mobile Safari/537.36");

		if($header) {
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);	
		}
		if($post) {
			curl_setopt($ch, CURLOPT_POST, true);
		}
		if($post_param) {
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_param);
		}
		if($cookie){
			curl_setopt($ch, CURLOPT_COOKIEFILE, getcwd().'/'.$cookie);
			curl_setopt($ch, CURLOPT_COOKIEJAR, getcwd().'/'.$cookie);
		}
		if($get_header) {
			curl_setopt($ch, CURLOPT_HEADER, 1);
		}
		
		$res = curl_exec($ch);
		$http = curl_getinfo($ch, CURLINFO_HTTP_CODE);   
		curl_close($ch);
		return array('code' => $http,'response'=>$res);
	}	
	
	public function instagram($url, $post=false, $post_param=false, $header_custom = false, $cookie=false, $proxy = false, $get_header = false, $json_cookie = false)
	{
		
		$header = array(
			'origin: https://www.instagram.com'
		);
		
		if($header_custom)
		{
			$header = array_merge($header,$header_custom);
		}
			
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://www.instagram.com/'.$url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Mobile Safari/537.36');
		if($proxy) {
			curl_setopt($ch, CURLOPT_PROXY, $proxy); 
		}
		if($post){
			curl_setopt($ch, CURLOPT_POST, true);	
		}
		if($post_param){
			if(is_array($post_param)){
				$post_param = http_build_query($post_param);	
			}
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_param);
		}
		if($cookie) {
			curl_setopt($ch, CURLOPT_COOKIEFILE, getcwd().'/'.$cookie);
			curl_setopt($ch, CURLOPT_COOKIEJAR, getcwd().'/'.$cookie);
		}
		if($get_header) {
			curl_setopt($ch, CURLOPT_HEADER, 1);
		}
		
		$res = curl_exec($ch);
		$http = curl_getinfo($ch, CURLINFO_HTTP_CODE);   
		curl_close($ch);
		if($json_cookie)
		{
			preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $res, $cookies);
			$results['cookie'] = $cookies[1];
		}
		$results['code'] = $http;
		$results['response'] = $res;
		return $results;
	}
	public function apiInstadaily($url, $post=false, $post_param=false)
	{
		
		$headers = array('Token: lJCbWloJ1SdvyI9WhLQEx8yHpZtSJiAv');
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'http://api.instadaily.pro/'.$url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
		if($post){
			curl_setopt($ch, CURLOPT_POST, true);	
		}
		if($post_param){
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_param);
		}
		
		$res = curl_exec($ch); 
		curl_close($ch);
		
		return $res;
	}
	public function apiInstagram($url, $post=false, $post_param=false, $header_custom = false, $proxy = false, $get_header = false)
	{
		
		$header = array(
			'origin: https://www.instagram.com'
		);
		
		if($header_custom) {
			$header = array_merge($header,$header_custom);
		}
			
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://www.instagram.com/'.$url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Mobile Safari/537.36');
		if($proxy) {
			curl_setopt($ch, CURLOPT_PROXY, $proxy); 
		}
		if($post){
			curl_setopt($ch, CURLOPT_POST, true);	
		}
		if($post_param){
			if(is_array($post_param)) {
				$post_param = http_build_query($post_param);
			}
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_param);
		}
		if($get_header) {
			curl_setopt($ch, CURLOPT_HEADER, 1);
		}
		
		$res  = curl_exec($ch);
		$http = curl_getinfo($ch, CURLINFO_HTTP_CODE);   
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		
		curl_close($ch);
		
		$return = array(
			'code'     => $http,
			'response' => $res
		);
		
		if($get_header)
		{
			$header = substr($res,0,$header_size);	
			$res    = trim(substr($res,$header_size));
			
			$return['response'] = $res;
			$return['header'] = $header;
			
		}
		
		return $return;
	}	
		
}
?>