<?php
 
class Image {  
private $_image;
private $_imageFormat;  
 
public function load($imageFile) {  
    $imageInfo = getImageSize($imageFile);
    $this->_imageFormat = $imageInfo[2];
    if( $this->_imageFormat === IMAGETYPE_JPEG ) {  
        $this->_image = imagecreatefromjpeg($imageFile);
    } elseif( $this->_imageFormat === IMAGETYPE_GIF ) { 
        $this->_image = imagecreatefromgif($imageFile);
    } elseif( $this->_imageFormat === IMAGETYPE_PNG ) { 
        $this->_image = imagecreatefrompng($imageFile);
    }
}
 
public function save($imageFile, $_imageFormat=IMAGETYPE_JPEG, $compression=75, $permissions=null) {  
    if( $_imageFormat == IMAGETYPE_JPEG ) {
        imagejpeg($this->_image,$imageFile,$compression);
    } elseif ( $_imageFormat == IMAGETYPE_GIF ) {  
        imagegif($this->_image,$imageFile);
    } elseif ( $_imageFormat == IMAGETYPE_PNG ) {  
        imagepng($this->_image,$imageFile);
    }
    if( $permissions != null) {  
        chmod($imageFile,$permissions);
    }
}
     
 
public function getWidth() {  
    return imagesx($this->_image);
}
 
public function getHeight() {  
    return imagesy($this->_image);
}
 
public function resizeToHeight($height) {  
    $ratio = $height / $this->getHeight();
    $width = $this->getWidth() * $ratio;
    $this->resize($width,$height);
}  
 
public function resizeToWidth($width) {
    $ratio = $width / $this->getWidth();
    $height = $this->getheight() * $ratio;
    $this->resize($width,$height);
}  
 
public function scale($scale) {
    $width = $this->getWidth() * $scale/100;
    $height = $this->getheight() * $scale/100;
    $this->resize($width,$height);
}  
 
public function resize($width, $height) {
    $newImage = imagecreatetruecolor($width, $height);
    imagecopyresampled($newImage, $this->_image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
    $this->_image = $newImage;
}  
 
}
 
?>