<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Schedule Document</title>

<meta name="robots" content="noindex">
<meta name="googlebot" content="noindex">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<style>
  .input-method
  {
    border:1px solid #ddd;
    border-radius:4px;
  }
  .input-method h5 
  {
    font-size: 15px;
    text-transform: uppercase;
    font-weight: 600;
  }
  .url-api
  {
    padding:6px 0;
    margin-top:0;
  }
  .url-api-link
  {
    color:#999;font-size:17px
  }
  .table-api 
  {
    margin-bottom: 40px;
  }
  h4
  {
    margin-top: 50px;
  }
</style>

</head>

<body>

<div class="container">
	<!-- container -->
	<h2 class="text-center">Documents Schedule</h2>
    <!-- Docs -->
  	<div>
    	<h4>I. GET</h4>
     	<div class="container input-method">
       	   <h5>Setting</h5>
           <div  class="url-api">
              <span class="url-api-link"> api/setting/get/{account-id}</span>
          </div>
          <table class="table table-api table-striped table-hover">
          <thead>
            <tr>
              <th>Name</th>
              <th>Null</th>
              <th>Default Value</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>fields</td>
              <td>Yes</td>
              <td>*</td>
              <td>Lấy Thông Tin Các Trường. Nếu để trống lấy tất cả các trường . ex: fields=id,schedule_id,..</td>
            </tr>
            
          </tbody>
        </table>
        <hr>
        	<h5>Source</h5>
        	<div class="url-api">
            	<span class="url-api-link">/api/source/get/{account-id}</span>
          </div>
        	<table class="table table-striped table-hover table-api">
          <thead>
            <tr>
              <th>Name</th>
              <th>Null</th>
              <th>Default Value</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>fields</td>
              <td>Yes</td>
              <td>*</td>
              <td>Lấy Thông Tin Các Trường. Nếu để trống lấy tất cả các trường . ex: fields=id,schedule_id,..</td>
            </tr>
            
          </tbody>
        </table>
    
		</div>
	</div>
  <!-- ENd  -->
  <!-- PUSH -->
  <div>
      <h4>II. PUSH </h4>
      <h5> <b>NOTE : </b>TẤT CẢ SỬ DỤNG METHOD POST</h5>
      <div class="container input-method">
      	 <h5>Setting</h5>
          <div class="url-api">
              <span class="url-api-link"> /api/setting/push</span>
          </div>
          <table class="table table-striped table-hover table-api">
          <thead>
            <tr>
              <th>Fields</th>
              <th>Type</th>
              <th>Null</th>
              <th>Default Value</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>acc_id</td>
              <td>Number</td>
              <td>No</td>
              <td>No</td>
              <td>Account ID</td>
            </tr>
            <tr>
              <td>between_post</td>
              <td>Json</td>
              <td>No</td>
              <td>No</td>
              <td>Thời Gian Ngẫu Nhiên Giữa Các Post ex: 3_5,5_9,12</td>
            </tr>
            <tr>
              <td>post_time_start</td>
              <td>Number</td>
              <td>No</td>
              <td>No</td>
              <td>Thời Gian Bắt Đầu Post</td>
            </tr>
            <tr>
              <td>post_time_end</td>
              <td>Number</td>
              <td>No</td>
              <td>No</td>
              <td>Thời Gian Kết Thúc Post</td>
            </tr>
            <tr>
              <td>total_post_in_day</td>
              <td>Json</td>
              <td>No</td>
              <td></td>
              <td>Tổng trong một ngày</td>
            </tr>
            <tr>
              <td>max_in_day</td>
              <td>Number</td>
              <td>Yes</td>
              <td>Null</td>
              <td>Lưu Hastag </td>
            </tr>
            <tr>
              <td>hastag</td>
              <td>String</td>
              <td>Yes</td>
              <td>Null</td>
              <td>Lưu Hastag </td>
            </tr>
            <tr>
              <td>remove_hastag</td>
              <td>Number</td>
              <td>Yes</td>
              <td>0</td>
              <td>Xóa tag trong Caption Post. Nhận giá trị 1 - 0 . 1->Có . 0->Không</td>
            </tr>
            <tr>
              <td>active_my_tag</td>
              <td>Number</td>
              <td>Yes</td>
              <td>0</td>
              <td>Xóa tag trong Caption Post. Nhận giá trị 1 - 0 . 1->Có . 0->Không</td>
            </tr>
            <tr>
              <td>random_hastag</td>
              <td>Number</td>
              <td>Yes</td>
              <td>0</td>
              <td>Nhặt ngẫu nhiên số lượng tag.</td>
            </tr>
            
          </tbody>
        </table>
        <hr>
        <h5>Source</h5>
         <div class="url-api">
              <span class="url-api-link">/api/source/push</span>
          </div>
          <table class="table table-striped table-hover table-api">
          <thead>
            <tr>
              <th>Fields</th>
              <th>Type</th>
              <th>Null</th>
              <th>Default Value</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
           <tr>
            <td>account_id</td>
            <td>Number</td>
            <td>No</td>
            <td>No</td>
            <td>Nhận Account ID chạy</td>
           </tr>
           <tr>
            <td>user</td>
            <td>String</td>
            <td>No</td>
            <td>No</td>
            <td>User Instagram</td>
           </tr>
           <tr>
            <td>id_ins</td>
            <td>Number</td>
            <td>Yes</td>
            <td>No</td>
            <td>ID cuả tài khoản Instagram</td>
           </tr>
           <tr>
            <td>followers</td>
            <td>Number</td>
            <td>No</td>
            <td>No</td>
            <td>Số người đang theo dõi</td>
           </tr>
           <tr>
            <td>following</td>
            <td>Number</td>
            <td>No</td>
            <td>No</td>
            <td>Đang Theo Dõi</td>
           </tr>
           <tr>
            <td>avt</td>
            <td>String</td>
            <td>Yes</td>
            <td>0</td>
            <td>Ảnh Đại Diện Của Account</td>
           </tr>
           <tr>
            <td>total_post</td>
            <td>Number</td>
            <td>No</td>
            <td>No</td>
            <td>Tổng số bài viết của tài khoản</td>
           </tr>
          </tbody>
        </table>
    </div>
  </div>
  <!-- End push -->
  
  <!-- Update -->
  <div>
      <h4>III. UPDATE </h4>
      <h5> <b>NOTE : </b>TẤT CẢ SỬ DỤNG METHOD POST</h5>
      <div class="container input-method">
      	  <h5>Setting</h5>
          <div class="url-api">
              <span class="url-api-link"> /api/setting/update/{account-id}</span>
          </div>
          <!-- Table -->
          <table class="table table-striped table-hover table-api">
          <thead>
            <tr>
              <th>Fields</th>
              <th>Type</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>acc_id</td>
              <td>Number</td>
              <td></td>
            </tr>
            <tr>
              <td>between_post</td>
              <td>String</td>
             <td></td>
            </tr>
            <tr>
              <td>post_time_start</td>
              <td>Number</td>
              <td></td>
            </tr>
            <tr>
              <td>post_time_end</td>
              <td>Number</td>
              <td></td>
            </tr>
            <tr>
              <td>total_post_in_day</td>
              <td>Number</td>
              <td></td>
            </tr>
            <tr>
              <td>hastag</td>
              <td>String</td>
              <td></td>
            </tr>
            <tr>
              <td>remove_hastag</td>
              <td>Number</td>
              <td></td>
            </tr>
            <tr>
              <td>random_hastag</td>
              <td>Number</td>
              <td></td>
            </tr>
          </tbody>
        </table>
          <hr>
          <h5>Source</h5>
          <div class="url-api">
              <span class="url-api-link"> /api/source/update/{id}</span>
          </div>
        <!-- Table -->
    </div>
  </div>
  <!-- End Update -->
  
  <!-- Delete -->
  <div>
      <h4>III. DELETE </h4>
      <div class="container input-method">
      	  <h5>Setting</h5>
          <div class="url-api">
              <span class="url-api-link"> /api/setting/delete/{account-id}</span>
          </div>
          <!-- Table -->
          <hr>
          <h5>Source</h5>
          <div class="url-api">
              <span class="url-api-link"> /api/source/delete/{id}</span>
          </div>
        <!-- Table -->
    </div>
  </div>
  <!-- End Delete -->
  

</div>
<!-- end container -->
</body>
</html>