<?php
class Api_model extends CI_Model{
	
	public function getSetting($select,$acc_id)
	{
		$where = array('acc_id'=>$acc_id);
		$data = $this->db->select($select)->where($where)->get('setting_schedule')->row_array();
		return $data;
	}

	public function getSource($select,$account_id)
	{
		$data['results']		= $this->db->select($select)->where(array('account_id'=>$account_id))->get('source_schedule')->result_array();
		$data['total_results']	= $this->db->select($select)->where(array('account_id'=>$account_id))->count_all_results('source_schedule');
		return $data;
	}

	private  function select_fields($table)
	{
	    $row = $this->db->list_fields($table);
	    return $row;
	}

	public function check_fields($table,$select)
	{
		$column = $this->select_fields($table);
		$fields = explode(',',$select);
		foreach ($fields as $key => $value) {
			if(!in_array($value,$column))
			{
				$this->error_fields('Field '.$value.' Fail');
				break;
			}
		}
		
		return $select;
	}
	public function check_exist($account_id,$field,$table,$exist = true)
	{
	    $rows = $this->db->where(array($field=>$account_id))->get($table)->num_rows();
	    if($exist == true && $rows > 0)
	    {
	    	echo response(400,'Object exist');
      		exit();
	    }else if ($exist == false && $rows == 0)
	    {
	    	echo response(400,"Object does't exist");
      		exit();
	    }
	}

	private function error_fields($error)
	{
	    echo '{"error":"'.$error.'"}';
	    exit();
	}
}?>