<!DOCTYPE html>
<html>
  <head>
    <title>Maintenance</title>
    <style>
	.maintenance{
		text-align:center;
		margin-top:100px;
		font-weight:600;
			color:#333;
			font-size:18px;	
	}
	img{
		background-position:center;	
	}
    </style>
  </head>
<body>
  <div class="maintenance">
  	<p><img src="assets/home/img/maintenance.png"></p>
    <p>We apologize but our site is currently undergoing maintenance at this time.</p>
    <p>Please check back later.</p>
  </div>   
</body>
</html>