<?php
function response($code, $message, $json=true) {
	if($code === false || !$message) {
		return '';	
	}
	
	if(is_array($message)) {
		$res = $message;
		$res['code'] = $code;
	}
	else{
		$res = array(
			'code' => $code,
			'message' => $message
		);
	}
	
	
	if($json == true) {
		header('Content-Type: application/json');
		$res = json_encode($res);	
	}	
	
	return $res;
}