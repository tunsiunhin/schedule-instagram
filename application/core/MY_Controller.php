<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class MY_Controller extends CI_Controller{
	public $cookie = array();
	public function __construct()
	{
		parent::__construct();
 	}
 	public function getAccount($param)
 	{
 		$this->load->library('curl');
 		$url = '/account/get';
 		$param = $param;
 		$res = $this->curl->apiInstadaily($url,true,$param);
		return json_decode($res,true);
 	}
	public function updateCookie($param)
	{
		$this->load->library('curl');
 		$url = '/account/update';
 		$res = $this->curl->apiInstadaily($url,true,$param);
		return json_decode($res,true);
	}
	public function updateTimeRun($param)
	{
		$this->load->library('curl');
 		$url = '/action/update';
 		$res = $this->curl->apiInstadaily($url,true,$param);
		return json_decode($res,true);
	}
	
	public function accountUpdate($param,$id)
	{
		$this->load->library('curl');
 		$url = '/account/update/'.$id;
 		$res = $this->curl->apiInstadaily($url,true,$param);
		return json_decode($res,true);
	}
	public function actionUpdate($param,$id)
	{
		$this->load->library('curl');
 		$url = '/action/update/'.$id;
 		$res = $this->curl->apiInstadaily($url,true,$param);
		return json_decode($res,true);
	}
}
?>