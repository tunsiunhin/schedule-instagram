<?php  (defined('BASEPATH')) OR exit('No direct script access allowed');
    class Get extends CI_Controller
    {
      public function __construct()
      {
        parent::__construct();
		$this->load->helper(array('response'));
      }
	  public function index($account_id)
	  {
	  	if(!$account_id)
		{
			 echo response(400,'Parameter error');
			 exit();
		}
	  	$results = array();
		$results['setting']	= $this->db->where('account_id',$account_id)->get('setting_schedule')->row_array();
		$results['source']	= $this->db->where('account_id',$account_id)->get('source_schedule')->result_array();
		echo response(200,$results);
	  }

	
}?>