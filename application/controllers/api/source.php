<?php  (defined('BASEPATH')) OR exit('No direct script access allowed');
    class Source extends CI_Controller
    {
    	public function __construct()
    	{
       parent::__construct();
       $this->load->model('api_model');
       $this->load->library('curl');
       $this->load->helper(array('response'));
     }
     public function index($method = '')
     {

      $account_id = $this->uri->segment(4);
      $post       = $this->input->post() ? $this->input->post() : '';

      if(empty($method))
      {
        echo response(400,'Error');
        exit();
      }

      switch ($method)
      {
        case 'get':
        $this->getSource($account_id);
        break;
        case 'push':
        $this->insertSource($account_id);
        break;
        case 'update':
        $this->update($account_id,$post);
        break;
        case 'delete':
        $this->delete($account_id);
        break;
        default:
        echo response(400,'Method error');
        break;
      }
    }

    private function getSource($account_id)
    {

      $fields     = $this->input->get('fields');
      $select     =  '*';
      if($fields || !empty($fields))
      {
        $select = $this->api_model->check_fields('source_schedule',$fields);
      };

      $data =  $this->api_model->getSource($select,$account_id);
      echo json_encode($data);
      
    }
    private function insertSource($account_id)
    {
	  
	 // $res= $this->db->where('account_id',$account_id)->get('source_schedule')->result_array();echo json_encode($res);die;
	 
      $source   = json_decode($this->input->post('source'),true);

      if(empty($source))
      {
        echo response(400,'Error Data');
        exit();
      }
	  
	  $account_id  = $account_id;
	  
	  if(!$account_id)
	  {
	  	echo response(400,"Opps Fail Save");
		exit();
	  }
	  
	  $old_source = $this->db->where('account_id',$account_id)->get('source_schedule')->result_array();
	  $array_pk   = array_column($old_source,'pk');;
	  if($old_source)
	  {
	  	foreach($source as $key => $value)
		{
			if(in_array($value['pk'],$array_pk))
			{
				unset($source[$key]);
				$array_pk = array_diff($array_pk, [$value['pk']]);

			}
		}
	  }
	  
	  if($source)
	  {
	  	$this->db->insert_batch('source_schedule', $source); 
	  }
	  if($array_pk)
	  {
	  	$this->db->where('account_id',$account_id)->where_in('pk',$array_pk)->delete('source_schedule');
	  }
      echo response(200,"Success");


    }
    private function update($account_id,$post)
    {

      if(empty($account_id) || empty($post))
      {
        echo response(400,'Parameter error');
        exit();
      }
      $this->api_model->check_exist($account_id,'account_id','source_schedule',false);
      $this->db->where('account_id',$account_id)->update('source_schedule',$post);
    }
    private function delete($id)
    {

      if(empty($id))
      {
        echo response(400,'Method error');
        exit();
      }
      $this->api_model->check_exist($id,'id','source_schedule',false);
      $this->db->where('id',$id)->delete('source_schedule');
      echo response(200,"Success");

    }
}?>	