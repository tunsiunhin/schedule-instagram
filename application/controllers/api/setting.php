<?php  (defined('BASEPATH')) OR exit('No direct script access allowed');
    class Setting extends CI_Controller
    {
      public function __construct()
      {
        parent::__construct();
        $this->load->model('api_model');
        $this->load->library('curl');
        $this->load->helper(array('response'));
      }
      public function index($method)
      {
        $account_id = $this->uri->segment(4);
        $post       = $this->input->post() ? $this->input->post() : '';

        if(!isset($method))
        {
          echo response(400,'Error');
          exit();
        }

        switch ($method)
        {
          case 'get':
          $this->getSetting($account_id);
          break;
          case 'push':
          $this->insertSetting($account_id,$post);
          break;
          case 'update':
          $this->update($account_id,$post);
          break;
          case 'delete':
          $this->delete($account_id);
          break;
          default:
          echo response(400,'Parameter error');
          break;
        }
      }
      public function getSetting($account_id)
      {

        if(empty($account_id))
        {
          echo response(400,'Error');
          exit();
        }

        $fields     = $this->input->get('fields');
        $select     =  '*';
        if($fields || !empty($fields))
        {
          $select = $this->api_model->check_fields('setting_schedule',$fields);
        };
        
        $data['result'] =  $this->api_model->getSetting($select,$account_id);
        echo json_encode($data);
      }
      public function insertSetting($account_id,$post)
      {

        if(empty($post))
        {
          echo response(400,'Method error');
          exit();
        }
		$check = $this->db->where('account_id',$account_id)->get('setting_schedule')->row_array();
		if($check)
		{
			$this->db->where('account_id',$account_id)->update('setting_schedule',$post);
		}else
		{
			$data['account_id'] = $account_id;
			$this->db->insert('setting_schedule',$data);
		}
        
        echo response(200,"Success");
      }
      public function update($account_id,$post)
      {

        if(empty($account_id) || empty($post))
        {
          echo response(400,'Method error');
          exit();
        }

        $this->api_model->check_exist($account_id,'acc_id','setting_schedule',false);
        $this->db->where('acc_id',$account_id)->update('setting_schedule',$post);
        echo response(200,"Success");
      }
      public function delete($account_id)
      {

        if(empty($account_id))
        {
          echo response(400,'Method error');
          exit();
        }

        $this->api_model->check_exist($account_id,'acc_id','setting_schedule',false);
        $this->db->where('acc_id',$account_id)->delete('setting_schedule');
        echo response(200,"Success");
      }


}?>