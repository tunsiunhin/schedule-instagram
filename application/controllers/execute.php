<?php  (defined('BASEPATH')) OR exit('No direct script access allowed');
class Execute extends MY_Controller
{
	public $acc_id;

	public function __construct()
	{
		parent::__construct();
	}
	
	
    public function index($id)
    {
 		if(empty($id)) {
			exit;	
		}
		
		$this->acc_id = $id;
		
		$param = array(
            'acc_id'    => $this->acc_id,
            'proxy'     => true,
            'action_id' => 1,
        );
		
        $res = $this->curl->instadaily('account/get', true, $param);
		
		$res = json_decode($res, true);
		
        if (empty($res['data'])) {
            exit;
        }
		
		$account = current($res['data']);
		
		$cookies = json_decode($account['cookies'], true);
        $proxy   = $account['proxy'];
		//$proxy   = '';
		
		$setting = $this->db->where('acc_id',$this->acc_id)->get('repost_setting')->row_array();
		
		$source = $this->get_source($cookies,$setting['id'],$proxy,$setting['posted_last_time']);
		
		if( !$source) {
			
			if($source === false) {
				$this->repost_error($account['aca_id']);	
			}
			
			exit;	
		}
		
		$caption = $this->get_content($source['caption'],$setting['remove_hashtag'],$setting['use_hashtag'],$setting['hashtag']);	
    	
		$upload_id = $this->upload($source['picture'],$cookies,$proxy);
		
		if( !$upload_id) {
			$this->repost_error($account['aca_id']);
			exit;	
		}

		$exec  = $this->exec_post($upload_id,$caption,$cookies,$proxy);
		
		if( !$exec) {
			$this->repost_error($account['aca_id']);
			exit;	
		}
		
		$update_action = array(
			'results' => $account['results']
		);
		
		$update_setting = array(
			'max_day' => $setting['max_day'],
		);
		
		$posted = array(
			'acc_id'   => $this->acc_id,
			'pk'       => $source['pk'],
			'media_id' => $source['media_id'],
			'type'     => $source['type'],
			'posted_time' => time()
		);
		
		$this->db->insert('repost_posted',$posted);
		
		foreach($exec['cookies'] as $key => $cookie) {
			$cookies[$key] = $cookie;
		}
					
		$update_action['results']++;
						
		$update_setting['max_day']--;
		
		if($update_setting['max_day'] <= 0) { //Đạt giới hạn post trong ngày
			$post_day = json_decode($setting['post_per_day'],true);
			$update_setting['max_day'] = rand($post_day[0],$post_day[1]);
			$update_action['time_run'] = $this->get_time($setting['delay_post'],$setting['execute_time'],2);
		}
		else {
			$time_run = $this->get_time($setting['delay_post'],$setting['execute_time'],1);
			$update_action['time_run'] = $time_run['nexttime'];
			if($time_run['new_day'] == true) {
				$post_day = json_decode($setting['post_per_day'],true);
				$update_setting['max_day'] = rand($post_day[0],$post_day[1]);
			}
		}
		
		$this->db->where('id',$setting['id'])->set($update_setting)->update('repost_setting');
		
		$this->curl->instadaily('action/update/'.$account['aca_id'],true,$update_action);
		
		$update_account = array(
			'cookies' => json_encode($cookies),
			'token'   => $cookies['csrftoken']
		);
		
	   $this->curl->instadaily('account/update/'.$this->acc_id, true, $update_account);
	   
	   
    }
	
	private function get_source($cookies,$setting_id,$proxy,$last_time)
	{
		$source = $this->db->where('setting_id',$setting_id)->order_by('last_post_time','desc')->limit(1)->get('repost_source')->row_array();
		
		$posted = $this->db->select('media_id')->where(array('pk'=>$source['pk'],'acc_id'=>$this->acc_id))->order_by('id','desc')->limit(500)->get('repost_posted')->result_array();
			
		$posted_id  = array_column($posted,'media_id');
		
		
		$header = array(
		  'x-csrftoken:'.$cookies['csrftoken'],
		  'cookie:'.$this->cookie_string($cookies)
		);
		
		$query_hash = 'df16f80848b2de5a3ca9495d781f98df';
		$param		= '?query_hash='.$query_hash.'&variables={"id":"'.$source['pk'].'","first":50,"after":""}';
		$url 		= 'graphql/query/'.$param;
		
		
		$res = $this->curl->instagram($url,false,false,$header,$proxy);
		
		
				
		$results  = json_decode($res['response'],true);
		
		if($res['code'] != 200 || json_last_error() !== JSON_ERROR_NONE || !isset($results['status']) || $results['status'] != 'ok')
		{
			$this->repost_log('source',$res['code'],'',$res['response']);
			return false;	
		}

		$posts 	  = $results['data']['user']['edge_owner_to_timeline_media']['edges'];
		
		$this->db->where('id',$source['id'])->set('last_post_time',time())->update('repost_source');
		
		foreach($posts as $post)
		{
			
			$post = $post['node'];
			
			if(in_array($post['id'],$posted_id) == true) {
				continue;	
			}
			if($last_time > 0 && $post['taken_at_timestamp'] < $last_time) {
				break;
			}
			
			return array(
				'pk'      => $source['pk'],
				'media_id'=>$post['id'],
				'picture' => current(end($post['display_resources'])),
				'caption' => $post['edge_media_to_caption']['edges'][0]['node']['text'],
				'type' 	  => $post['is_video'] ? 2 : 1,
				'posted_time' => $post['taken_at_timestamp'], 
			);
			
		}
		
		return array();
	}
	private function get_content($caption,$remove_hashtag,$use_hashtag,$hashtag)
	{
		if($remove_hashtag) {
		 	$caption = preg_replace('/(#\w+)/','',$caption);
		}
		
		if($use_hashtag == 1 && $hashtag != '') {
			$hashtag  = str_replace(' ','',$hashtag);
			$hashtag  = str_replace(',',' #',$hashtag);
			$caption .= '#'.$hashtag;
			
		}
		
		return $caption;
	}
    public function upload($image,$cookies,$proxy)
    {

		 $boundary = uniqid();
		 $delimiter = '----WebKitFormBoundary' . $boundary;
		 $upload_id = time().'000';
		 $media_type = 1;
		 $file_type = pathinfo($image);
		 $eol = "\r\n";
		 $type_picture = explode('?', $file_type['extension']);
		 $file = file_get_contents($image);
	
	
		 if($type_picture['0'] == 'png')
		 {
		   $data = imagecreatefromstring($file);
		   ob_start();
		   imagejpeg($data, null, 100);
		   $file = ob_get_contents();
		   ob_end_clean();
		 }
	
		 $param  =  '--'.$delimiter.$eol;
		 $param .=  'Content-Disposition: form-data; name="upload_id"'.$eol.$eol.$upload_id.$eol;
		 $param .=  '--'.$delimiter.$eol;
		 $param .=  'Content-Disposition: form-data; name="media_type"'.$eol.$eol.$media_type.$eol;
		 $param .=  '--'.$delimiter.$eol;
		 $param .=  'Content-Disposition: form-data; name="photo" filename="photo.jpg"'.$eol.$eol.$file.$eol;
		 $param .=  '--'.$delimiter.'--';
	
	
		 $url = "create/upload/photo/";
	
		 $header = array(
		  'content-type: multipart/form-data; boundary='.$delimiter,
		  'Content-length:'.strlen($param),
		  'x-csrftoken:'.$cookies['csrftoken'],
		  'referer: https://www.instagram.com/create/style/',
		  'x-requested-with: XMLHttpRequest',
		  'cookie:'.$this->cookie_string($cookies)
		);
		
		
		$res = $this->curl->instagram($url,true,$param,$header,$proxy);
		
		$response = json_decode($res['response'],true);
		
		if($res['code'] != 200 || json_last_error() !== JSON_ERROR_NONE || !isset($response['status']) || $response['status'] != 'ok')
		{
			$this->repost_log('upload',$res['code'],'',$res['response']);
			
			return false;
		}
		
		return $response['upload_id'];
		
   }

    public function exec_post($upload_id,$caption,$cookies,$proxy)
    {
		 $header = array(
		  'content-type: application/x-www-form-urlencoded',
		  'x-csrftoken:'.$cookies['csrftoken'],
		  'referer: https://www.instagram.com/create/details/',
		  'x-requested-with: XMLHttpRequest',
		  'cookie:'.$this->cookie_string($cookies)
		);

     	$param = array('upload_id'=>$upload_id,'caption'=>$caption);
     	

     	$res = $this->curl->instagram('create/configure/',true,$param,$header,$proxy,true);
		
		$response = json_decode($res['response'],true);
		
		if($res['code'] != 200 || json_last_error() !== JSON_ERROR_NONE || !isset($response['status']) || $response['status'] != 'ok')
		{
			$this->repost_log('exec',$res['code'],$res['header'],$res['response']);
			return false;
		}
		
		return array(
			'success' => true,
			'cookies' => $this->get_cookie($res['header'])
		);	
		
    }
	
	
	private function repost_log($type,$code,$header,$result)
	{
		$log = array(
			'type'    => $type,
			'acc_id'  => $this->acc_id,
			'code'	  => $code,
			'header'  => $header,
			'result'  => $result,
			'created' => time(),
		);
		
		$this->db->insert('repost_log',$log);
	}
	
	private function repost_error($aca_id)
	{
		$update_action = array(
			'error' => 1
		);
		$this->curl->instadaily('action/update/'.$aca_id, true, $update_action);	
	}
	
	private function get_time($delay,$exec_time,$type)
	{
		$timetoday = strtotime('TODAY');
		
		$exec_time = json_decode($exec_time,true);

		$start = $timetoday + $exec_time[0] * 3600;
			
		$end   = $timetoday + $exec_time[1] * 3600; 
		
		if($end < $start) 
		{
			if(intval(date('H')) < $exec_time[0]) {
				$start -= 86400;	
			} 
			else {
				$end += 86400;	
			}
			
		}
		
		if($type == 1) {
			
			$delay  = json_decode($delay,true);
			$delay  = rand($delay[0],$delay[1]) * 60;
			
			$nexttime = $delay + time();			
			
			$new_day = false;
						
			if($nexttime >= $end) {
				$nexttime = $start + 86400;	
				$new_day = true;
			}
			
			return array('nexttime'=>$nexttime,'new_day'=>$new_day);
		}
		
		else {
			$nexttime = $start + 86400;	
			return $nexttime;
		}
		
	}

    private function get_cookie($header)
    {
      preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $header, $matches);
	  
      $cookies = array();
      foreach ($matches[1] as $item) {
        parse_str($item, $cookie);
        if( !trim(str_replace('"','',current($cookie))) ) {
          continue;	
        }
        $cookies = array_merge($cookies, $cookie);
      }

      return $cookies;
    }
	
    private function cookie_string($cookies)
    {
      $cookie_string = '';
      foreach ($cookies as $key => $cookie) {

        $cookie_string .= $key . '=' . $cookie . ';';
      }
      return $cookie_string;
    }

}?>	
