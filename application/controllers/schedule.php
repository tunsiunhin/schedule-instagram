<?php  (defined('BASEPATH')) OR exit('No direct script access allowed');
    class Schedule extends MY_Controller
    {
    	public function __construct()
    	{
       parent::__construct();
       $this->load->library('curl');
       $this->load->helper(array('response'));
       date_default_timezone_set("Asia/Ho_Chi_Minh");
     }
     public function start()
     {
 
		 $account_id = $this->input->get('account_id');
		 $user_id    = $this->input->get('user_id');
		 
		 if(!$account_id || !$user_id)
		 {
			echo response(400,'Param Error. Line 23');
			exit();
		 }
		 
		 $bigAccount = $this->getAccount(array('acc_id'=>$account_id,'user_id'=>$user_id,'proxy'=>true,'action_id'=>1));
		 if(!isset($bigAccount['data'][0]))
		 {
		   echo response(400,'Not found data. Line 32');
		   exit();
		 }
		 
		 $account = $bigAccount['data'][0];
		 
		 $max_post = $this->time_run($account_id,true);
		 
		 if($max_post)
		 {
			$this->execute_next($max_post['time'],$account['aca_id'],false);
			echo response(200,'Limited In Day');
			exit();
		 }
		 
	
		 $setting = $this->db->where('account_id',$account_id)->get('setting_schedule')->row_array();
	
		 if(!$account || !$setting)
		 {
		   echo response(400,'Error . Line 49');
		   exit();
		 }
		 
		$source = $this->get_source($account_id);
		
		if(!isset($source['picture']))
		{
		  echo response(400,'Not Picture');
		  exit();
		}
		
		$source['caption'] = $this->caption_process($setting,$source['caption']);		
    	$this->execute($source['picture'],$source['caption'],$account,$setting);
		

    }
    public function execute($image = '',$caption = '', $account,$setting)
    {

		 $arrayCookie = json_decode($account['cookies'],true);
		 $strCookie   = $this->cookie_string($arrayCookie);
		 $csrftoken   = $account['token'];
		
		 $boundary = uniqid();
		 $delimiter = '----WebKitFormBoundary' . $boundary;
		 $upload_id = time().'000';
		 $media_type = 1;
		 $file_type = pathinfo($image);
		 $eol = "\r\n";
		 $type_picture = explode('?', $file_type['extension']);
		 $file = file_get_contents($image);
	
	
		 if($type_picture['0'] == 'png')
		 {
		   $data = imagecreatefromstring($file);
		   ob_start();
		   imagejpeg($data, null, 100);
		   $file = ob_get_contents();
		   ob_end_clean();
		 }
	
		 $param  =  '--'.$delimiter.$eol;
		 $param .=  'Content-Disposition: form-data; name="upload_id"'.$eol.$eol.$upload_id.$eol;
		 $param .=  '--'.$delimiter.$eol;
		 $param .=  'Content-Disposition: form-data; name="media_type"'.$eol.$eol.$media_type.$eol;
		 $param .=  '--'.$delimiter.$eol;
		 $param .=  'Content-Disposition: form-data; name="photo" filename="photo.jpg"'.$eol.$eol.$file.$eol;
		 $param .=  '--'.$delimiter.'--';
	
	
		 $url = "create/upload/photo/";
	
		 $header = array(
		  'content-type: multipart/form-data; boundary='.$delimiter,
		  'Content-length:'.strlen($param),
		  'x-csrftoken:'.$csrftoken,
		  'referer: https://www.instagram.com/create/style/',
		  'x-requested-with: XMLHttpRequest',
		  'cookie:'.$strCookie
		);
		
		
		$res = $this->curl->instagram($url,true,$param,$header,false,$account['proxy']);
	
		if($res['code'] != 200)
		{
			// Ghi Log
			$this->schedule_log($account['id'],$res['code'],'','','photo',$account['aca_id']);
			echo response(400,'Not Push Instargram. Code '.$res['code']);
			exit();
		}
		
		$this->upload($upload_id,$caption,$account,$setting);
     
   }

    public function upload($upload_id,$caption,$account,$setting)
    {
          //Upload ảnh khi đã có id
		  
		 $url        = 'create/configure/';
		 
		 $arrayCookie = json_decode($account['cookies'],true);
		 $strCookie   = $this->cookie_string($arrayCookie);
		 $csrftoken   = $account['token'];
		 
		 $header = array(
		  'content-type: application/x-www-form-urlencoded',
		  'x-csrftoken:'.$csrftoken,
		  'referer: https://www.instagram.com/create/details/',
		  'x-requested-with: XMLHttpRequest',
		  'cookie:'.$strCookie
		);

     	$param = array('upload_id'=>$upload_id,'caption'=>$caption);
     	$param= http_build_query($param);

     	$res = $this->curl->instagram($url,true,$param,$header,false,$account['proxy'],true,false);

		if($res['code'] == 200)
		{
		   $cookies = $this->get_cookie($res['response']);
		   
		   foreach ($cookies as $kc => $vc)
		   {
				$arrayCookie[$kc] = $vc;
		   }
		   //update Cookie
		   $j_cookie = json_encode($arrayCookie);
		   $res_cookie = $this->accountUpdate(array('cookies'=>$j_cookie,'token'=>$csrftoken),$account['id']);
		   
		   //update time
		   $time = $this->time_run($account['id']);
		   $this->execute_next($time['time'],$account['aca_id'],true,$account['results']);
		   
		   //update setting

		   $setting['max_in_day']-- ;
		   $this->db->where('account_id',$account['id'])->update('setting_schedule',array('max_in_day'=>$setting['max_in_day']));
		   
		   echo response(200,'Successs');
		   exit(); 
		 }else
		 {
			 $this->schedule_log($account['id'],$res['code'],'','','photo',$account['aca_id']);
			 echo response(400,'Error 251 .Code '.$res['code']);
			 exit(); 
		 }
    }
	
	// function support
	public function schedule_log($account_id,$code,$message = '',$time,$type = 'photo',$error_id = false)
	{
		if($message == '')
		{
			$message = 'Null Reponse';
		}
		$data = array(
			'account_id' => $account_id,
			'code'		 => $code,
			'message'	 => $message,
			'time'		 => time(),
			'type'		 => $type
		);
		
		$this->db->insert('logs_schedule',$data);
		
		if($error_id)
		{
			$this->actionUpdate(array('error'=>1),$error_id);
		}
	}
    public function time_run($acc_id,$max_post = false)
    {

      $today    = strtotime('TODAY');
      $time     = time();
      $hour     = date('H',$time);

      $row      =  $this->db->where('account_id',$acc_id)->get('setting_schedule')->row_array();
	  $execute_time	 =  json_decode($row['execute_time']);
	  $row['post_time_start'] = $execute_time[0] ? $execute_time[0] : 0;
	  $row['post_time_end']	  = $execute_time[1] ? $execute_time[1] : 23;
	  $data 	= array();
      if($max_post)
      {
        $total    = json_decode($row['total_post_in_day']);

        if($row['max_in_day'] <= 0)
        {
           $max_in_day = rand($total[0],$total[1]);

           if($row['post_time_start'] <= $row['post_time_end'])
           {

              $time_next  =  $today + 86400 + ($row['post_time_start'] * 3600);
      
           }
           else
           {
              
              if($hour <= $row['post_time_start'])
              {
                $time_next  =  $today + ($row['post_time_start'] * 3600);
              }else
              {
                $time_next  =  $today + 86400 + ($row['post_time_start'] * 3600);
              }
           }

           $this->db->update('setting_schedule',array('max_in_day'=>$max_in_day));
           $data['time'] = $time_next;
        }// max in day
       	
      }else
      {
		  $bp = json_decode($row['between_post']);
		  $between_post = rand($bp[0],$bp[1]);
		  $start = strtotime($row['post_time_start'].':00');
		  $end = strtotime($row['post_time_end'].':00'); 
		  
          if($row['post_time_start'] <= $row['post_time_end'])
          {
           		$time_next = time() + ($between_post * 60);
				if($time_next > $end)
				{
					$time_next = $today + 86400 + ($row['post_time_start'] * 3600) + ($between_post * 60);
				}
				else if($time_next < $start)
				{
					$time_next = $today + ($row['post_time_start'] * 3600) + ($between_post * 60);
				}
          }else
		  {
			  	$time_next = time() + ($between_post * 60);
				
		  		if($time_next >= $end && $time_next <= $start)
				{
					$time_next = $today + ($row['post_time_start'] * 3600) + ($between_post * 60);
				}
				
		  }
		 $data['time'] = $time_next;
      }
		
	  return $data;
	  
    }
	public function caption_process($setting,$caption)
	{
		if($setting['remove_hastag'])
		{
		 
		 	$caption = preg_replace('/(#\w+)/','',$caption);
		
		}
		// Lọc Thêm Sửa Xóa hastag mới
		if($setting['active_my_tag'])
		{
		  if(!empty($setting['hastag']))
		  {
			 $arr_hastag = explode(' ',$setting['hastag']);
			 $caption = $this->mix_caption($caption,$arr_hastag);
	
		  }//end if hastag 
	
		}//end active_my_tag
		return $caption;
	}
	public function get_source($account_id)
	{
		$rows = $this->db->where('account_id',$account_id)->order_by('scan_time','desc')->get('source_schedule')->result_array();
		$data = array();
		foreach ($rows as $row)
		{
		  $url = 'https://www.instagram.com/'.$row['username'];
		  $result = $this->read_posts($url);
				// Nếu Đã Từng Lấy Post thì Bỏ Qua hoặc Không Có
		  if(empty($result[0]['node']) || $result[0]['node']['id'] == $row['last_post'])
			continue;
	
		  $data['picture'] = $result[0]['node']['thumbnail_src'] ? $result[0]['node']['thumbnail_src'] : '';
		  $data['caption'] = $result[0]['node']['edge_media_to_caption']['edges'][0]['node']['text'] ? $result[0]['node']['edge_media_to_caption']['edges'][0]['node']['text'] : '';
		  $this->db->where('id',$row['id'])->set(array('last_post'=>$result[0]['node']['id'],'scan_account'=>$row['scan_account']+1,'scan_time'=>time()))->update('source_schedule');														    	  // Break Vòng lặp
		  break;
		}
		return $data;
	}
    public function read_posts($url)
    {
    		// Đọc Tường User
     $res = $this->curl->call($url);
     preg_match('/(window._sharedData =)(.*);/',$res['response'],$results);
     $json = ($results[2]) ? json_decode($results[2],true) : '';
     $posts =($json['entry_data']['ProfilePage']['0']['graphql']['user']['edge_owner_to_timeline_media']['edges']) ? $json['entry_data']['ProfilePage']['0']['graphql']['user']['edge_owner_to_timeline_media']['edges'] : '';
     
	 return $posts;
    }

    private function execute_next($time,$id, $rs = false ,$result = 0)
    {
	  // next
	  $data = array('time_run'=>$time);
	  if($rs)
	  {
	  	$data['results'] = $result + 1;
	  }
	  	
      $res = $this->actionUpdate($data,$id);
    
    }
	
    private function mix_caption($caption,$array_hastag)
    {
      $eol = "\r\n";
      foreach ($array_hastag as $key => $value) {
        if($key%2 == 0)
        {
          $caption .= $eol.$value;
        }else
        {
          $caption .= ' '.$value;
        }
      }
      return $caption;
    }
	
    private function get_cookie($header)
    {
      preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $header, $matches);
	  
      $cookies = array();
      foreach ($matches[1] as $item) {
        parse_str($item, $cookie);
        if( !trim(str_replace('"','',current($cookie))) ) {
          continue;	
        }
        $cookies = array_merge($cookies, $cookie);
      }

      return $cookies;
    }
    private function cookie_string($cookies)
    {
      $cookie_string = '';
      foreach ($cookies as $key => $cookie) {

        $cookie_string .= $key . '=' . $cookie . ';';
      }
      return $cookie_string;
    }

}?>	