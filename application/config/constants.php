<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);
define('MAINTENANCE', 0);


/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');
/* localhost */
/*define('APP_ID',     '520345978331592');
define('APP_SECRET', '751386cc9cdf1c76c039e903cbee0728');*/

/* server */
define('APP_ID',     '1755631491397028');
define('APP_SECRET', '1cec4468781e170ec7c066cdcae7b502');


define('_PASSKEY', 'thienglieng');
define('_MD5SALT', 'rt43rf2DaHuaeKoko');
define('COOKIE_NEXTTIME',300);
define('_RECAPTCHA_KEY', '6LcKfzAUAAAAACc9dKEz3KYv4-lObTEKFgkGPdkn');
define('_RECAPTCHA_SECRET','6LcKfzAUAAAAAE-tD6f6oJqkaI7_BUWZgoaYkNtx');