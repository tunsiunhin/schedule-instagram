<?php defined('BASEPATH') OR exit('No direct script access allowed');
$route['404_override']         = 'app/notfound';
$route['translate_uri_dashes'] = TRUE;
$route['default_controller']   = 'dev/index';
$route['docs']				   = 'dev/docs';
$route['api/setting/(:any)']   = 'api/setting/index/$1';
$route['api/source/(:any)']	   = 'api/source/index/$1';

$route['api/get/(:any)']	   = 'api/get/index/$1';

